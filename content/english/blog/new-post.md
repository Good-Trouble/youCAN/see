---
title: "my first"
date: 2022-03-20T06:32:52-05:00
image : "images/blog/newplot.png"
author: John Doe
bg_image: "images/blog/newplot.png"
categories: ["Problems"]
tags: ["Advice","Technology"]
draft: false
---

As shown in Fig. P3.25, Refrigerant 134a is contained in a piston–cylinder assembly, initially as saturated vapor. The refrigerant is slowly heated until its temperature is 160 °C. During the process, the piston moves smoothly in the cylinder.

![image](https://i.imgur.com/11oMMd1.png)

For the refrigerant, evaluate the work, in kJ/kg.


[notebook](https://good-trouble.gitlab.io/youCAN/doThermodynamics/plutons/ms9.03.025.html)




