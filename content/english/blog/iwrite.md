---
title: "I write"
author: "mcs"
image: false
categories: ["Artificial soup","pronk"]
tags: ["Advice","Retro","AI","ploop"]
date: 2022-05-12T01:39:02-05:00
draft: false
---

I write the blogs

\begin{gather}
\require{physics}
m^2 \dv t
\end{gather}


# Solution: Isochoric adiabatic charging process

## Given

An evacuated vessel container (V = 2 m³) is connected by a valve to an air supply (15 bar, 350 K). Initially, the valve is closed. The valve is opened, and air flows into the vessel until its pressure equals line pressure.

![test-four-isochoric-charging.svg](assets/images/test-four-isochoric-charging.svg)


## Find

1. Final temperature of the air in the vessel 

   > Continuity   
   > Conservation of energy

1. Why is the final temperature lower/higher/equal to the supply temperature?     

   > Higher temperature. Flow energy is converted to internal energy


## Solution

We have air, which we can safely assume is behaving ideally. 
With no heat transfer information given, we assume the 
vessel is adiabatic.
This is a classic unsteady flow scenario, so we'll start 
with mass conservation, then move to the energy equation.

### Mass

No mass exits, and the vessel is evacuated at the start.

$$
\begin{gather}
  m_{in} &-& m_{out} &=& \Delta m_{sys} \\\\
  m_{in} & &         &=& m_{final}  \\
\end{gather}
$$

### Energy
The vessel is rigid and adiabatic, and the vessel is 
evacuated at the start. The vessel cannot lose energy.

$$
\begin{gather}
  E_{in} &-& E_{out} &=& \Delta E_{sys} \\\\
  m_{in} h_{in} &&   &=& m_{final} u_{final}   \\
\end{gather}
$$

**Combining both equations** yields

$$
  h_{in} = u_{final}  
$$

